package main

import "fmt"

func main() {

	err := LoadConfig()
	if err != nil {
		fmt.Println("No config found. Exiting!")
	} else {
		fmt.Println("Hi", Name(), "!")
	}
}
