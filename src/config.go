package main

import "github.com/kelseyhightower/envconfig"

type generalConfig struct {
	Name string `required:"true"`
}

var (
	general generalConfig
)

func LoadConfig() error {
	var config generalConfig
	err := envconfig.Process("", &config)

	general = config
	return err
}

func Name() string {
	return general.Name
}
